<?php
	include_once "/../Config/Database.php";
	
	
	class Model_penerbit {
			
		public function getData()
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * FROM table_penerbit ORDER BY nama ASC";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		
		public function create() 
		{
			$db = new Database();
			$dbConnect = $db->connect();
			
			$sql = "INSERT INTO table_penerbit
					(
						nama,
						alamat,
						telepon,
						whatsapp,
					)
					VALUES
					(
						'{$this->nama}',
						'{$this->alamat}',
						'{$this->telepon}',
						'{$this->whatsapp}'
					)";
			
			$data = $dbConnect->query($sql);
			$error = $dbConnect->error;
			$dbConnect = $db->close();
			return $error;
		}
		
		public function hapus()
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "DELETE FROM table_penerbit where id_penerbit = '{$this->id_penerbit}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
		}

		
		
	
	}
	?>