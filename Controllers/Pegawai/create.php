<?php
	include "/../../Model/Model_pegawai.php";
	$pegawai = new Model_pegawai();
	
	//mengisi attribute dengan hasil dari inputan
	$pegawai->nik = $_POST['nik'];
	$pegawai->nama_depan = $_POST['nama_depan'];
	$pegawai->nama_belakang = $_POST['nama_belakang'];
	$pegawai->alamat = $_POST['alamat'];
	$pegawai->email = $_POST['email'];
	$pegawai->password = $_POST['password'];
	$pegawai->photo_profile = "https://ssl.gstatic.com/images/branding/product/1x/avatar_square_blue_512dp.png";
	$pegawai->jenis_kelamin = $_POST['jenis_kelamin'];
	$pegawai->bagian = $_POST['bagian'];
	//menampung gasil dari method create_function
	$error = $pegawai->create();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error) {
		//memanggil tampilan detail dengan mengirimkann page dan nrp
		header("location: ../../index.php?page=data-pegawai");
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=data-pegawai");
	}
?>