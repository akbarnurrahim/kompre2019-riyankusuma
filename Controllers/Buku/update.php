<?php
	
	include "/../../Model/Model_buku.php";
	$buku = new Model_buku();
	
	//mengisi attribute dengan hasil dari inputan
	$buku->id_buku = $_POST['id_buku'];
	$buku->isbn = $_POST['isbn'];
	$buku->judul_buku = $_POST['judul_buku'];
	$buku->gambar = $_POST['gambar'];
	$buku->pengarang = $_POST['pengarang'];
	$buku->penerbit = $_POST['penerbit'];
	$buku->tahun_terbit = $_POST['tahun_terbit'];
	$buku->jumlah_halaman = $_POST['jumlah_halaman'];
	$buku->deskripsi = $_POST['deskripsi'];
	$buku->berat = $_POST['berat'];
	$buku->kategori = $_POST['kategori'];
	$buku->harga_eceran = $_POST['harga_eceran'];
	$buku->stok = $_POST['stok'];

	
	//menampung gasil dari method create_function
	$error = $buku->update();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error)
	{
		header("location: ../../index.php?page=data-buku");
	} else {
		session_start();
		$_SESSION['message'] = $error;
		header("location: ../../../index.php?page=data-buku");
	}
	
?>