<?php
	include "/../../Model/Model_distributor.php";
	$distributor = new Model_distributor();
	
	//mengisi attribute dengan hasil dari inputan
	$distributor->nama = $_POST['nama'];
	$distributor->alamat = $_POST['alamat'];
	$distributor->telepon = $_POST['telepon'];
	$distributor->whatsapp = $_POST['whatsapp'];
	$distributor->email = $_POST['email'];
	//menampung gasil dari method create_function
	$error = $distributor->create();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error) {
		//memanggil tampilan detail dengan mengirimkann page dan nrp
		header("location: ../../index.php?page=data-distributor");
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=data-distributor");
	}
?>