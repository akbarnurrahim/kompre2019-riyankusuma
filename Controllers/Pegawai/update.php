<?php
	
	include "/../../Model/Model_pegawai.php";
	$pegawai = new Model_pegawai();
	
	//mengisi attribute dengan hasil dari inputan
	$pegawai->nama_depan = $_POST['nama_depan'];
	$pegawai->nama_belakang = $_POST['nama_belakang'];
	$pegawai->alamat = $_POST['alamat'];
	$pegawai->email = $_POST['email'];
	$pegawai->password = $_POST['password'];
	$pegawai->jenis_kelamin = $_POST['jenis_kelamin'];
	$pegawai->hak_akses = $_POST['hak_akses'];

	//menampung gasil dari method create_function
	$error = $pegawai->update();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error)
	{
		header("location: ../../index.php?page=data-pegawai");
	} else {
		session_start();
		$_SESSION['message'] = $error;
		header("location: ../../../index.php?page=data-pegawai");
	}
	
?>