<?php
	include "/../../Model/Model_kategori.php";
	$kategori = new Model_kategori();
	
	//mengisi attribute dengan hasil dari inputan

	$kategori->nama = $_POST['nama'];

	//menampung gasil dari method create_function
	$error = $kategori->create();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error) {
		//memanggil tampilan detail dengan mengirimkann page dan nrp
		header("location: ../../index.php?page=input-kategori-buku");
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=input-kategori-buku");
	}
?>