<?php
	include_once "/../Config/Database.php";
	
	
	class Model_buku {
			
		public function getData()
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * FROM table_buku ORDER BY judul_buku ASC";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		
		public function getDetail($id_buku)
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * FROM table_buku where id_buku = '{$id_buku}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data->fetch_array();
		}
		
		public function create() 
		{
			$db = new Database();
			$dbConnect = $db->connect();
			
			$sql = "INSERT INTO table_buku
					(
						isbn,
						judul_buku,
						gambar,
						pengarang,
						penerbit,
						tahun_terbit,
						jumlah_halaman,
						deskripsi,
						kategori,
						harga_eceran,
						stok
					)
					VALUES
					(
						'{$this->isbn}',
						'{$this->judul_buku}',
						'{$this->gambar}',
						'{$this->pengarang}',
						'{$this->penerbit}',
						'{$this->tahun_terbit}',
						'{$this->jumlah_halaman}',
						'{$this->deskripsi}',
						'{$this->kategori}',
						'{$this->harga_eceran}',
						'{$this->stok}'
					)";
			
			$data = $dbConnect->query($sql);
			
			$error = $dbConnect->error;
			$dbConnect = $db->close();
			return $error;
		}
		
		
		public function hapus()
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "DELETE FROM table_buku where id_buku = '{$this->id_buku}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
		}

		
		
	
	}
	?>