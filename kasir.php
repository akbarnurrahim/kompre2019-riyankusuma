<?php
    session_start()
?>
 <?php 
  include "Model/Model_buku.php";
  include "Model/Model_kategori.php";


  $buku = new Model_buku();


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Slim Responsive Bootstrap 4 Admin Template</title>

    <!-- vendor css -->
    <link href="Public/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="Public/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="Public/lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="Public/css/slim.css">
     <link href="Public/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="Public/css/bootstrap-combobox.css" media="screen" rel="stylesheet" type="text/css">

  </head>
  <body>
    <div class="slim-header">
      <div class="container">
        <div class="slim-header-left">
          <h2 class="slim-logo"><a href="index.html">Bukuku<span>.</span></a></h2>

        </div><!-- slim-header-left -->
        <div class="slim-header-right">
          
         
          <div class="dropdown dropdown-c">
            <a href="#" class="logged-user" data-toggle="dropdown">
               <img src="<?php echo $_SESSION['photo_profile'] ?> alt="">
               <span><?php echo $_SESSION['nama_depan'] ?>  <?php echo $_SESSION['nama_belakang'] ?></span>
              <i class="fa fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <nav class="nav">
                <a href="page-profile.html" class="nav-link"><i class="icon ion-person"></i> View Profile</a>
                 <a href="Controllers/Login/keluar.php" class="nav-link"><i class="icon ion-forward"></i> Keluar</a>
              </nav>
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
        </div><!-- header-right -->
      </div><!-- container -->
    </div><!-- slim-header -->

    <div class="slim-navbar" >
      <div class="container">
        <ul class="nav" >
          <li class="nav-item with-sub " >
            <a class="nav-link" href="#">
              <i class="icon ion-ios-home-outline"></i>
              <span>Penjualan</span>
            </a>
            <div class="sub-item" style="width: 100%">
              <ul>
                <li><a href="index.html">History Pembelian</a></li>
              </ul>
            </div><!-- sub-item -->
          </li>
      
        </ul>
      </div><!-- container -->
    </div><!-- slim-navbar -->

  

        <div class="row row-sm mg-t-20">
          <div class="col-lg-4">
            <div class="section-wrapper">
             
              <div class="form-layout form-layout-4">
                <div class="row">
                 
                  <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                    <select name='a' class='combobox form-control' >
                      <option value=''> Ketikan Kode ISBN / Judul Buku</option>
                     <?php $result = $buku->getData(); ?>
                     <?php while ($data = $result->Fetch_assoc()): ?>
                     <?php $judul = $data['judul_buku']; ?>
                     <?php $isbn= $data['isbn']; ?>
                    <option value="<?php echo $judul ?>"><?php echo $isbn?> - <?php echo $judul ?></option>
                     <?php endwhile; ?>
                    </select>
                  </div>
                </div><!-- row -->
                <div class="row mg-t-20">
              
                  <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                    <input type="text" class="form-control" disabled value="">
                  </div>
                </div>
                 <div class="row mg-t-20">
              
                  <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                    <input type="text" class="form-control" disabled value="">
                  </div>
                </div>
                <div class="row mg-t-20">
                
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input type="text" class="form-control" placeholder="Jumlah Beli">
                  </div>
                    &nbsp;
                    &nbsp;

                    &nbsp;
                    <buttton class="col-sm-3 btn btn-primary">Tambah&nbsp;<i class="icon ion-ios-add"></i></buttton>
                </div>
                <div class="form-layout-footer mg-t-30">
                  <button style="width:100%;" class="btn btn-primary bd-0">Selesai&nbsp;<i class="icon ion-ios-cart-outline"></i></button>
                  <br>
                  <br>
                </div><!-- form-layout-footer -->
              </div><!-- form-layout -->
            </div><!-- section-wrapper -->
          </div><!-- col-6 -->

          <div class="col-lg-8 mg-t-20 mg-lg-t-0">
            <div class="section-wrapper">
                <div class="d-flex align-items-center  bg-gray-100 ht-md-80 bd pd-x-20">
            <div class="d-md-flex pd-y-20 pd-md-y-0">
              <h1>TOTAL : Rp.20.000</h1>
          </div>

      </div>

              
            </div><!-- section-wrapper -->
            <br>
           
            <div class="section-wrapper">

            <div class="row">

  <div class="col-12 col-sm-12 col-lg-12">
   <div class="table-responsive">
    <table class="table" id="myTable">
     <thead>
      <tr>
        <th>#</th>
         <th>ISBN
        </th>
        <th>Judul Buku
        </th>
        <th>Harga
        </th>
        <th>Qty</th>
        <th>Sub Total</th>
        <th>Options</th>
      </tr>
    </thead>
    <tbody>
        <td>1</td>
        <td>923213123213123</td>
        <td>Dilan 1990</td>
        <td>Rp.28000</td>
       <td>2</td>
       <td>Rp.56000</td>
        <td><button class="btn btn-danger"><span class="ion ion-remove"></span></button></td>
      </tr>
 
    </tbody>
  </table>
            </div>
          </div><!-- col-6 -->

       
        </div><!-- row -->



</div>
    </div>


      </div><!-- container -->
    </div><!-- slim-mainpanel -->

    <div class="slim-footer">
      <div class="container">
        <p>Copyright 2018 &copy; All Rights Reserved. Slim Dashboard Template</p>
        <p>Designed by: <a href="">ThemePixels</a></p>
      </div><!-- container -->
    </div><!-- slim-footer -->

   

    <script src="Public/lib/jquery/js/jquery.js"></script>
    <script src="Public/lib/popper.js/js/popper.js"></script>

    <script src="Public/lib/bootstrap/js/bootstrap.js"></script>
    <script src="Public/lib/jquery.cookie/js/jquery.cookie.js"></script>
    <script src="Public/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="Public/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="Public/js/bootstrap-combobox.js" type="text/javascript"></script>
    <script src="Public/lib/select2/js/select2.min.js"></script>
    
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    <script src="Public/js/slim.js"></script>
     <script >
  
    $(document).ready(function(){
    $('.combobox').combobox();
  });
  </script>
 
    
      <script>
    $(document).ready( function () {
    $('#myTable').DataTable( {
      "dom": 'ltrip',
      "bPaginate": false
    });
    } );


</script>

  </body>
</html>
