  <?php 
    include_once "Config/Database.php";
    $tanggal  = date("Y-m-d");
    $db1 = new Database();
    $dbConnect1 = $db1->connect();

    // Menghitung Jumlah Kolom Yang Ada Di tabel Buku
    $sql_buku = mysqli_query($dbConnect1,"SELECT * FROM table_buku");
    $total_buku = mysqli_num_rows($sql_buku); 

    // Menghitung Jumlah Kolom Yang Ada Di tabel Distributor
    $sql_distributor = mysqli_query($dbConnect1,"SELECT * FROM table_distributor");
    $total_distributor = mysqli_num_rows($sql_distributor); 

    $sql_order = mysqli_query($dbConnect1,"SELECT * FROM table_order");
    $total_order = mysqli_num_rows($sql_order); 
  ?>
     <div class="card card-dash-one mg-t-20">
            <div class="row no-gutters">
              <div class="col-lg-3">
                <i class="icon ion-ios-book"></i>
                <div class="dash-content">
                  <label class="tx-primary">Jumlah Buku</label>
                  <h2><?php echo $total_buku ?></h2>
                </div><!-- dash-content -->
              </div><!-- col-3 -->
              <div class="col-lg-3">
                <i class="icon ion-ios-pie-outline"></i>
                <div class="dash-content">
                  <label class="tx-success">Total Penjualan</label>
                  <h2><?php echo $total_order ?></h2>
                </div><!-- dash-content -->
              </div><!-- col-3 -->
              <div class="col-lg-3">
                <i class="icon ion-ios-stopwatch-outline"></i>
                <div class="dash-content">
                  <label class="tx-purple">Jumlah Distributor</label>
                  <h2><?php echo $total_distributor ?></h2>
                </div><!-- dash-content -->
              </div><!-- col-3 -->
              <div class="col-lg-3">
                <i class="icon ion-ios-world-outline"></i>
                <div class="dash-content">
                  <label class="tx-danger">Earnings</label>
                  <h2>369,657</h2>
                </div><!-- dash-content -->
              </div><!-- col-3 -->
            </div><!-- row -->
          </div><!-- card -->

          <div class="row row-sm mg-t-20">
            <div class="col-xl-6">
              <div class="card card-table">
                <div class="card-header">
                  <h6 class="slim-card-title">Buku Terlaris</h6>
                </div><!-- card-header -->
                <div class="table-responsive">
                  <table class="table mg-b-0 tx-13">
                    <thead>
                      <tr class="tx-10">
                        <th class="wd-10p pd-y-5">&nbsp;</th>
                        <th class="pd-y-5">Judul</th>
                        <th class="pd-y-5 tx-right">Terjual</th>
                        <th class="pd-y-5">Frekuensi</th>

                      </tr>
                    </thead>
                    <tbody>

                      <tr>
                        <td class="pd-l-20">
                          <img src="https://hmp.me/cpp5" class="wd-55" alt="Image">
                        </td>
                        <td>
                          <a href="" class="tx-inverse tx-14 tx-medium d-block">Dilan 1990</a>
                          <span class="tx-11 d-block"><span class="square-8 bg-danger mg-r-5 rounded-circle"></span> Kosong </span>
                        </td>
                        <td class="valign-middle tx-right">3,345</td>
                        <td class="valign-middle"><span class="tx-success"><i class="icon ion-android-arrow-up mg-r-5"></i>33.34%</span></td>
                
                      </tr>
                      <tr>
                        <td class="pd-l-20">
                          <img src="https://hmp.me/cpp6" class="wd-55" alt="Image">
                        </td>
                        <td>
                          <a href="" class="tx-inverse tx-14 tx-medium d-block">Cinta Itu Motivasi</a>
                          <span class="tx-11 d-block"><span class="square-8 bg-success mg-r-5 rounded-circle"></span>Tersedia</span>
                        </td>
                        <td class="valign-middle tx-right">720</td>
                        <td class="valign-middle"><span class="tx-danger"><i class="icon ion-android-arrow-down mg-r-5"></i>21.20%</span> </td>
                  
                      </tr>
                      </tbody>
                  </table>
                </div><!-- table-responsive -->
                <div class="card-footer tx-12 pd-y-15 bg-transparent">
                  <a href=""><i class="fa fa-angle-down mg-r-5"></i>Lihat Semua </a>
                </div><!-- card-footer -->
              </div><!-- card -->
            </div><!-- col-6 -->
            <div class="col-xl-6 mg-t-20 mg-xl-t-0">
              <div class="card card-table">
                <div class="card-header">
                  <h6 class="slim-card-title">User Transaction History</h6>
                </div><!-- card-header -->
                <div class="table-responsive">
                  <table class="table mg-b-0 tx-13">
                    <thead>
                      <tr class="tx-10">
                        <th class="wd-10p pd-y-5">&nbsp;</th>
                        <th class="pd-y-5">User</th>
                        <th class="pd-y-5">Type</th>
                        <th class="pd-y-5">Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="pd-l-20">
                          <img src="http://via.placeholder.com/500x500" class="wd-36 rounded-circle" alt="Image">
                        </td>
                        <td>
                          <a href="" class="tx-inverse tx-14 tx-medium d-block">Mark K. Peters</a>
                          <span class="tx-11 d-block">TRANSID: 1234567890</span>
                        </td>
                        <td class="tx-12">
                          <span class="square-8 bg-success mg-r-5 rounded-circle"></span> Email verified
                        </td>
                        <td>Just Now</td>
                      </tr>
               
                    </tbody>
                  </table>
                </div><!-- table-responsive -->
                <div class="card-footer tx-12 pd-y-15 bg-transparent">
                  <a href=""><i class="fa fa-angle-down mg-r-5"></i>View All Transaction History</a>
                </div><!-- card-footer -->
              </div><!-- card -->
            </div><!-- col-6 -->
          </div><!-- row -->
          <br>

     
  <div class="dash-headline">
            <div class="dash-headline-left">
              <div class="dash-headline-item-one">
                <div id="chartArea1" class="dash-chartist"></div>
                <div class="dash-item-overlay">
                  <h1>200.000 <span class="tx-24">RP</span></h1>
                  <p class="earning-label">Pendapatan Bulanan</p>
                  <p class="earning-desc">asdsad</p>
                  <a href="#" class="statement-link">Data Lengkap<i class="fa fa-angle-right mg-l-5"></i></a>
                </div>
              </div><!-- dash-headline-item-one -->
            </div><!-- dash-headline-left -->

            <div class="dash-headline-right">
              <div class="dash-headline-right-top">
                <div class="dash-headline-item-two">
                  <div id="chartMultiBar1" class="chart-rickshaw"></div>
                  <div class="dash-item-overlay">
                    <h4>45.000<span class="tx-20">RP</span></h4>
                    <p class="item-label">Pendapatan Harian</p>
                    <p class="item-desc">Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncusPublic.</p>
                    <a href="#" class="report-link">View Report <i class="fa fa-angle-right mg-l-5"></i></a>
                  </div>
                </div><!-- dash-headline-item-two -->
              </div><!-- dash-headline-right-top -->
              <div class="dash-headline-right-bottom">
                <div class="dash-headline-right-bottom-left">
                  <div class="dash-headline-item-three">
                    <span id="sparkline3" class="sparkline wd-100p">1,4,4,7,5,9,10,5,4,4,7,5,9,10</span>
                    <div>
                      <h1>29,931</h1>
                      <p class="item-label">Jumlah Pemasukan</p>
                      <p class="item-desc">Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncusPublic.</p>
                    </div>
                  </div><!-- dash-headline-item-three -->
                </div><!-- dash-headline-right-bottom-left -->
                <div class="dash-headline-right-bottom-right">
                  <div class="dash-headline-item-three">
                    <span id="sparkline4" class="sparkline wd-100p">1,4,4,7,5,7,4,3,4,4,6,5,9,7</span>
                    <div>
                      <h1>45,231</h1>
                      <p class="item-label">Jumlah Pengeluaran</p>
                      <p class="item-desc">Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncusPublic.</p>
                    </div>
                  </div><!-- dash-headline-item-three -->
                </div><!-- dash-headline-right-bottom-right -->
              </div><!-- dash-headline-right-bottom -->
            </div><!-- wd-50p -->
          </div><!-- d-flex ht-100v -->



                </div><!-- row -->

             
              </div><!-- card -->

              <div class="card card-impression mg-t-20">

                <div id="rs3" class="ht-50 ht-sm-70 mg-r--1"></div>
              </div><!-- card -->

          </div><!-- row -->
