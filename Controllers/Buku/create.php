<?php
	include "../../Model/Model_buku.php";
	$buku = new Model_buku();
	
	//mengisi attribute dengan hasil dari inputan
		
	// ambil data file
	$namaFile = $_FILES['gambar']['name'];
	$namaSementara = $_FILES['gambar']['tmp_name'];

	// tentukan lokasi file akan dipindahkan
	$dirUpload = "../../Img/";

	// pindahkan file
	$terupload = move_uploaded_file($namaSementara, $dirUpload.$namaFile);

	$buku->isbn = $_POST['isbn'];
	$buku->judul_buku = $_POST['judul_buku'];
	$buku->gambar = $namaFile;
	$buku->pengarang = $_POST['pengarang'];
	$buku->penerbit = $_POST['penerbit'];
	$buku->tahun_terbit = $_POST['tahun_terbit'];
	$buku->jumlah_halaman = $_POST['jumlah_halaman'];
	$buku->deskripsi = $_POST['deskripsi'];
	$buku->kategori = $_POST['kategori'];
	$buku->harga_eceran = $_POST['harga_eceran'];
	$buku->stok = $_POST['stok'];
	//menampung gasil dari method create_function
	$error = $buku->create();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error) {
		//memanggil tampilan detail dengan mengirimkann page dan nrp
		header("location: ../../index.php?page=data-buku");
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan create kembali
		echo '<script language="JavaScript" type="text/JavaScript">alert("Telah Terjadi Error! "); window.location =  "../../index.php?page=data-buku";</script> ';
	}
?>