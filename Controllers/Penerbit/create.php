<?php
	include "/../../Model/Model_penerbit.php";
	$penerbit = new Model_penerbit();
	
	//mengisi attribute dengan hasil dari inputan
	$penerbit->nama = $_POST['nama'];
	$penerbit->alamat = $_POST['alamat'];
	$penerbit->telepon = $_POST['telepon'];
	$penerbit->whatsapp = $_POST['whatsapp'];
	$penerbit->email = $_POST['email'];
	//menampung gasil dari method create_function
	$error = $penerbit->create();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error) {
		//memanggil tampilan detail dengan mengirimkann page dan nrp
		header("location: ../../index.php?page=data-penerbit");
	} else {
		//membuat session untuk menampilkan pesan error bernama message
		session_start();
		$_SESSION['message'] = $error;
		//memanggil tampilan create kembali
		header("location: ../../index.php?page=data-penerbit");
	}
?>