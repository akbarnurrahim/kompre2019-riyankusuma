 <div class="section-wrapper">
            <label class="section-title">Form Input Distributor</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan distributor sesuai dengan data yang ada</p>

            <div class="form-layout">
              <div class="row mg-b-25">
                <div class="col-lg-8">
				 <form action="Controllers/Distributor/create.php" method="post">
                  <div class="form-group">
                    <label class="form-control-label">Nama Distributor: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="nama">
                  </div>
                </div><!-- col-4 -->
                 <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Telepon: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="telepon">
                  </div>
                </div><!-- col-4 -->
                 <div class="col-lg-8">
                  <div class="form-group">
                    <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="email" name="email">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Nomer Whatsapp: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="whatsapp">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Alamat : <span class="tx-danger">*</span></label>
                    <textarea cols="10" rows="5" class="form-control" name="alamat"></textarea>
                  </div>
                </div><!-- col-4 -->
               
                 
                  
                
                           <div class="col-lg-12">
                  <div class="form-group">
                    
                    <button class="btn btn-primary bd-0">Simpan</button>
                  </div>
				  </form>
                </div><!-- col-4 -->
                

              </div><!-- row -->
            </div>
          </div>

          

                </div><!-- row -->

             
              </div><!-- card -->

              <div class="card card-impression mg-t-20">

                <div id="rs3" class="ht-50 ht-sm-70 mg-r--1"></div>
              </div><!-- card -->

          </div><!-- row -->