 <?php 
  include "Model/Model_pegawai.php";
  $pegawai = new Model_pegawai();
?>

 <div class="section-wrapper">
            <label class="section-title">Data pegawai</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan pegawai sesuai dengan data yang ada</p>

          
            <div class="row">

  <div class="col-12 col-sm-12 col-lg-12">
   <div class="table-responsive">
    <table class="table" id="myTable">
     <thead>
      <tr>
        <th>#</th>
        <th>Photo Profil
        </th>
        <th>Nama pegawai</th>
        <th>Nik</th>
        <th>Bagian</th>
        <th>Detail</th>
        <th>Print</th>
        <th>Update</th>
		<th>Hapus</th>
      </tr>
    </thead>
    <tbody>
      <?php $result = $pegawai->getData(); ?>
      <?php $cek = mysqli_num_rows($result); 
      $i=0;

      ?>
    
      <?php while($data = $result->fetch_array()):  ?>

      <tr>
        <?php $i++ ?>
        <td>1</td>
        <td><img src="<?php echo $data['photo_profile'] ?>"  class="wd-90"></td>
        <td><?php echo $data['nama_depan'] ?>&nbsp;<?php echo $data['nama_belakang'] ?></td>
        <td><?php echo $data['nik'] ?></td>
        <td><?php echo $data['bagian'] ?></td>
        <td><button class="btn btn-primary">Lihat Detail</button></td>
        <td><button class="btn btn-warning">Print</button></td>
        <td><form action="index.php?page=edit-pegawai&id_buku=<?php echo $data['id_pegawai'] ?>" method="post">
			<button onclick="return confirm('Anda Yakin Akan Menghapus Data ini?');" class="btn btn-success">Edit</button></form></td>
		<td><form action="Controllers/Pegawai/delete.php" method="post">
			<input type="hidden" value="<?php echo $data['id_pegawai'] ?>" name="id_pegawai">
			<button onclick="return confirm('Anda Yakin Akan Menghapus Data ini?');" class="btn btn-danger">Hapus</button></form></td>
			
      </tr>
    <?php endwhile; ?>
    </tbody>
  </table>
  </div>
</div>
