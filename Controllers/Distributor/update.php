<?php
	
	include "/../../Model/Model_distributor.php";
	$distributor = new Model_distributor();
	
	//mengisi attribute dengan hasil dari inputan
	$distributor->id_distributor = $_POST['id_distributor'];
	$distributor->nama = $_POST['nama'];
	$distributor->alamat = $_POST['alamat'];
	$distributor->telepon = $_POST['telepon'];
	$distributor->whatsapp = $_POST['whatsapp'];
	$distributor->email = $_POST['email'];


	//menampung gasil dari method create_function
	$error = $distributor->update();
	
	//pengecekan error atau berhasil, !$error = berhasil
	if(!$error)
	{
		header("location: ../../index.php?page=data-distributor");
	} else {
		session_start();
		$_SESSION['message'] = $error;
		header("location: ../../../index.php?page=data-distributor");
	}
	
?>