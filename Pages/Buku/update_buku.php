 <?php 
  include "Model/Model_penerbit.php";
  include "Model/Model_kategori.php";
  include "Model/Model_buku.php";

  $penerbit = new Model_penerbit();
  $kategori = new Model_kategori();
  $buku = new Model_buku();
?>

<?php 
	$data = null;
	if (isset($_GET['id_buku'])){
		$data = $buku->getDetail($_GET['id_buku']);
	}
?>
 <div class="section-wrapper">
            <label class="section-title">Form Input Buku</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan buku sesuai dengan data yang ada</p>

            <div class="form-layout">
              <form action="Controllers/Buku/create.php" method="post" enctype="multipart/form-data">
              <div class="row mg-b-25">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Judul Buku: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="judul_buku" value="<?php echo $data['judul_buku'] ?>">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">ISBN : <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="isbn" value="<?php echo $data['isbn'] ?>">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Penerbit: <span class="tx-danger">*</span></label>
                    <select class="form-control select2" name="penerbit" data-placeholder="Choose country">
					<option value="<?php echo $data['penerbit'] ?>"><?php echo $data['penerbit'] ?></option>
                     <?php $result = $penerbit->getData(); ?>
                     <?php while ($data2 = $result->Fetch_assoc()): ?>
                     <?php $nama2 = $data2['nama']; ?>
					
                    <option value="<?php echo $nama2 ?>"><?php echo $nama2 ?></option>
                      <?php endwhile; ?>
                    </select>
                  </div>
                </div><!-- col-4 -->
                  <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Pengarang : <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="pengarang" value="<?php echo $data['pengarang'] ?>">
                  </div>
                </div><!-- col-4 -->
                   <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Tahun Terbit: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="date" name="tahun_terbit" value="<?php echo $data['tahun_terbit'] ?>">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Jumlah Halaman: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="number" name="jumlah_halaman" value="<?php echo $data['jumlah_halaman'] ?>">
                  </div>
                </div><!-- col-4 -->
                  
                   <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Cover Buku: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="file" name="gambar">
                  </div>
                </div>
               <div class="col-lg-4">
                  <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Kategori: <span class="tx-danger">*</span></label>
					
                    <select class="form-control select2" data-placeholder="kategori">
					<option value="<?php echo $data['kategori'] ?>"><?php echo $data['kategori'] ?></option>
                     <?php $result = $kategori->getData(); ?>
                     <?php while ($data3 = $result->Fetch_assoc()): ?>
                     <?php $nama3 = $data3['nama']; ?>
					 <?php endwhile; ?>
                    <option value="<?php echo $nama3 ?>"><?php echo $nama3 ?></option>
                     
                    </select>
                  </div>
                </div><!-- col-4 -->
                 <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label">Harga Eceran: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="harga_eceran" value="<?php echo $data['harga_eceran'] ?>">
                  </div>
                </div><!-- col-4 -->
                 <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label">Stok: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="stok" value="<?php echo $data['stok'] ?>">
                  </div>
                </div><!-- col-4 -->
                      <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Deskripsi Buku (sinopsis): <span class="tx-danger">*</span></label>
                    <textarea cols="10" rows="9" class="form-control" name="deskripsi"><?php echo $data['deskripsi'] ?></textarea>
                  </div>
                </div><!-- col-4 -->
                           <div class="col-lg-12">
                  <div class="form-group">
					
                    <button class="btn btn-primary bd-0">Simpan</button>
					</form>
                  </div>
                </div><!-- col-4 -->
                

              </div><!-- row -->
            </div>
          </div>

          

                </div><!-- row -->

             
              </div><!-- card -->

              <div class="card card-impression mg-t-20">

                <div id="rs3" class="ht-50 ht-sm-70 mg-r--1"></div>
              </div><!-- card -->

          </div><!-- row -->