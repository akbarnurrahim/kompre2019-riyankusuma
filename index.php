<!DOCTYPE html>
<?php
  include_once "Config/Autoclose.php";
?>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php
    array_key_exists('page',$_GET) ? $page = $_GET['page'] : $page = 'halaman-utama';
    ?>
    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Aplikasi Penjualan Buku</title>

    <!-- vendor css -->
    <link href="Public/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="Public/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
    <link href="Public/lib/chartist/css/chartist.css" rel="stylesheet">
    <link href="Public/lib/rickshaw/css/rickshaw.min.css" rel="stylesheet">
    <link href="Public/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="Public/css/slim.css">
    <div id="hilang">
          <div id="chartMultiBar1" class="chart-rickshaw"></div>
           
               


          <div id="rs3" class="ht-50 ht-sm-70 mg-r--1"></div>
        </div>
  </head>
  <body>

    <div class="slim-header with-sidebar">
      <div class="container-fluid">
        <div class="slim-header-left">
          <h2 class="slim-logo"><a href="index.php?page=halaman-utama">Bukuku<span>.</span></a></h2>
          <a href="" id="slimSidebarMenu" class="slim-sidebar-menu"><span></span></a>
        
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
          <div class="dropdown dropdown-a" data-toggle="tooltip" title="Activity Logs">
            <a href="" class="header-notification" data-toggle="dropdown">
              <i class="icon ion-ios-bolt-outline"></i>
            </a>
            <div class="dropdown-menu">
              <div class="dropdown-menu-header">
                <h6 class="dropdown-menu-title">Log Aktifitas</h6>
                <div>
                  <a href="">Pengaturan</a>
                </div>
              </div><!-- dropdown-menu-header -->
              <div class="dropdown-activity-list">
                <div class="activity-label">Today, December 13, 2017</div>
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">10:15am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                    <div class="col-8">Purchased christmas sale cloud storage</div>
                  </div><!-- row -->
                </div><!-- activity-item -->
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">9:48am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-danger"></span></div>
                    <div class="col-8">Login failure</div>
                  </div><!-- row -->
                </div><!-- activity-item -->
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">7:29am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-warning"></span></div>
                    <div class="col-8">(D:) Storage almost full</div>
                  </div><!-- row -->
                </div><!-- activity-item -->
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">3:21am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                    <div class="col-8">1 item sold <strong>Christmas bundle</strong></div>
                  </div><!-- row -->
                </div><!-- activity-item -->
                <div class="activity-label">Yesterday, December 12, 2017</div>
                <div class="activity-item">
                  <div class="row no-gutters">
                    <div class="col-2 tx-right">6:57am</div>
                    <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                    <div class="col-8">Earn new badge <strong>Elite Author</strong></div>
                  </div><!-- row -->
                </div><!-- activity-item -->
              </div><!-- dropdown-activity-list -->
              <div class="dropdown-list-footer">
                <a href="page-activity.html"><i class="fa fa-angle-down"></i> Show All Activities</a>
              </div>
            </div><!-- dropdown-menu-right -->
          </div><!-- dropdown -->
          <div class="dropdown dropdown-b" data-toggle="tooltip" title="Notifications">
            <a href="" class="header-notification" data-toggle="dropdown">
              <i class="icon ion-ios-bell-outline"></i>
              <span class="indicator"></span>
            </a>
            <div class="dropdown-menu">
              <div class="dropdown-menu-header">
                <h6 class="dropdown-menu-title">Pemberitahuan Terbaru</h6>
                <div>
                  <a href="">Pengaturan</a>
                </div>
              </div><!-- dropdown-menu-header -->
              <div class="dropdown-list">
                <!-- loop starts here -->
                <a href="" class="dropdown-link">
                  <div class="media">
                    <img src="http://via.placeholder.com/500x500" alt="">
                    <div class="media-body">
                      <p><strong>Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                      <span>October 03, 2017 8:45am</span>
                    </div>
                  </div><!-- media -->
                </a>
                <!-- loop ends here -->
                <a href="" class="dropdown-link">
                  <div class="media">
                    <img src="http://via.placeholder.com/500x500" alt="">
                    <div class="media-body">
                      <p><strong>Mellisa Brown</strong> appreciated your work <strong>The Social Network</strong></p>
                      <span>October 02, 2017 12:44am</span>
                    </div>
                  </div><!-- media -->
                </a>
                <a href="" class="dropdown-link read">
                  <div class="media">
                    <img src="http://via.placeholder.com/500x500" alt="">
                    <div class="media-body">
                      <p>20+ new items added are for sale in your <strong>Sale Group</strong></p>
                      <span>October 01, 2017 10:20pm</span>
                    </div>
                  </div><!-- media -->
                </a>
                <a href="" class="dropdown-link read">
                  <div class="media">
                    <img src="http://via.placeholder.com/500x500" alt="">
                    <div class="media-body">
                      <p><strong>Julius Erving</strong> wants to connect with you on your conversation with <strong>Ronnie Mara</strong></p>
                      <span>October 01, 2017 6:08pm</span>
                    </div>
                  </div><!-- media -->
                </a>
                <div class="dropdown-list-footer">
                  <a href="page-notifications.html"><i class="fa fa-angle-down"></i> Show All Notifications</a>
                </div>
              </div><!-- dropdown-list -->
            </div><!-- dropdown-menu-right -->
          </div><!-- dropdown -->
          <div class="dropdown dropdown-c">
            <a href="#" class="logged-user" data-toggle="dropdown">
              <img src="<?php echo $_SESSION['photo_profile'] ?>" alt="">
              <span><?php echo $_SESSION['nama_depan'] ?>  <?php echo $_SESSION['nama_belakang'] ?></span>
              <i class="fa fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <nav class="nav">
               
                <a href="index.php?page=ubah-password" class="nav-link"><i class="icon ion-ios-gear"></i> Ubah Password</a>
                <a href="Controllers/Login/keluar.php" class="nav-link"><i class="icon ion-forward"></i> Keluar</a>
              </nav>
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
        </div><!-- header-right -->
      </div><!-- container-fluid -->
    </div><!-- slim-header -->

    <div class="slim-body">
      <div class="slim-sidebar">
        <label class="sidebar-label">Aplikasi Penjualan Buku</label>

        <ul class="nav nav-sidebar">
          <li class="sidebar-nav-item with-sub">
            <a href="index.php?page=halaman-utama" class="sidebar-nav-link <?php if (!$page ||$page=='halaman-utama' || $page=='') echo 'active'; ?>"><i class="icon ion-ios-home-outline"></i>Halaman Utama</a>
            
          </li>

             <li class="sidebar-nav-item with-sub">
            <a href="" class="sidebar-nav-link <?php if (!$page ||$page=='input-buku' || $page=='data-buku' || $page=='input-kategori-buku') echo 'active'; ?>"><i class="icon ion-ios-book-outline"></i> Buku</a>
            <ul class="nav sidebar-nav-sub">
              <li class="nav-sub-item"><a href="index.php?page=input-buku" class="nav-sub-link <?php if (!$page ||$page=='input-buku') echo 'active'; ?>">Input Buku</a></li>
              <li class="nav-sub-item"><a href="index.php?page=data-buku" class="nav-sub-link <?php if (!$page ||$page=='data-buku') echo 'active'; ?>">Data Buku</a></li>
			  <li class="nav-sub-item"><a href="index.php?page=update-stok-buku" class="nav-sub-link <?php if (!$page ||$page=='update-stok-buku') echo 'active'; ?>">Update Stok Buku</a></li>
			  <li class="nav-sub-item"><a href="index.php?page=input-kerusakan-buku" class="nav-sub-link <?php if (!$page ||$page=='input-kerusakan-buku') echo 'active'; ?>">Input Kerusakan Buku</a></li>
              <li class="nav-sub-item"><a href="index.php?page=input-kategori-buku" class="nav-sub-link <?php if (!$page || $page=='input-kategori-buku') echo 'active'; ?>">Input Kategori Buku</a></li>
              
              </ul>
            </ul>
          </li>

            <li class="sidebar-nav-item with-sub">
            <a href="" class="sidebar-nav-link <?php if (!$page ||$page=='input-distributor' || $page=='data-distributor') echo 'active'; ?>"><i class="icon ion-ios-star-outline"></i> Distributor</a>
            <ul class="nav sidebar-nav-sub">
              <li class="nav-sub-item"><a href="index.php?page=input-distributor" class="nav-sub-link <?php if (!$page || $page=='input-distributor') echo 'active'; ?>">Input Distributor</a></li>
              <li class="nav-sub-item"><a href="index.php?page=data-distributor" class="nav-sub-link <?php if (!$page || $page=='data-distributor') echo 'active'; ?>">Data Distributor</a></li>             
            </ul>
          </li>

      
           <li class="sidebar-nav-item with-sub">
            <a href="" class="sidebar-nav-link <?php if (!$page ||$page=='input-pegawai' || $page=='data-pegawai') echo 'active'; ?>"><i class="ion-ios-people-outline"></i> Pegawai</a>
            <ul class="nav sidebar-nav-sub">
              <li class="nav-sub-item"><a href="index.php?page=input-pegawai" class="nav-sub-link <?php if (!$page || $page=='input-pegawai') echo 'active'; ?>">Input Pegawai</a></li>
              <li class="nav-sub-item"><a href="index.php?page=data-pegawai" class="nav-sub-link <?php if (!$page || $page=='data-pegawai') echo 'active'; ?>">Data Pegawai</a></li>             
            </ul>
          </li>




         
         
          <li class="sidebar-nav-item with-sub">
            <a href="" class="sidebar-nav-link"><i class="icon ion-ios-gear-outline"></i> Pengaturan</a>
            <ul class="nav sidebar-nav-sub">
              <li class="nav-sub-item"><a href="page-profile.html" class="nav-sub-link">Edit Info Perusahaan</a></li>
            
            </ul>
          </li>
          

          <li class="sidebar-nav-item with-sub">
            <a href="" class="sidebar-nav-link"><i class="icon ion-ios-paper-outline"></i> Laporan</a>
            <ul class="nav sidebar-nav-sub">
              <li class="nav-sub-item"><a href="page-profile.html" class="nav-sub-link">Harian</a></li>
              <li class="nav-sub-item"><a href="page-invoice.html" class="nav-sub-link">Bulanan</a></li>
              <li class="nav-sub-item"><a href="page-contact.html" class="nav-sub-link">Tahunan</a></li>             
            </ul>
          </li>
         
       
          <li class="sidebar-nav-item with-sub">
            <a href="" class="sidebar-nav-link"><i class="icon ion-ios-information-outline"></i> Penjualan</a>
            <ul class="nav sidebar-nav-sub">
              <li class="nav-sub-item"><a href="util-background.html" class="nav-sub-link">Background</a></li>
              
            </ul>
          </li>
         
        </ul>
      </div><!-- slim-sidebar -->

      <div class="slim-mainpanel">
        <div class="container">
          <?php
                if($page=='halaman-utama')
                {
                  include "Pages/index/halaman-utama.php";
                }
                if ($page == '')
                {
                  include "Pages/halaman-utama.php";
                } 

                if ($page == 'input-buku')
                {
                  include "Pages/Buku/Input_buku.php";
                } 
                if ($page == 'data-buku')
                {
                  include "Pages/Buku/Data_buku.php";
                }   
                if ($page == 'input-kategori-buku')
                {
                  include "Pages/Buku/Input_kategori_buku.php";
                }


                if ($page == 'input-distributor')
                {
                  include "Pages/Distributor/Input_distributor.php";
                }
                if ($page == 'data-distributor')
                {
                  include "Pages/Distributor/Data_distributor.php";
                } 

                if ($page == 'input-penerbit')
                {
                  include "Pages/Penerbit/Input_penerbit.php";
                }
                if ($page == 'data-penerbit')
                {
                  include "Pages/Penerbit/Data_penerbit.php";
                } 

                if ($page == 'input-pegawai')
                {
                  include "Pages/Pegawai/Input_pegawai.php";
                }
                if ($page == 'data-pegawai')
                {
                  include "Pages/Pegawai/Data_pegawai.php";
                } 
                if ($page == 'ubah-password')
                {
                  include "Pages/Pegawai/Ubah_password.php";
                } 
				
				if ($page == 'edit-buku')
                {
                  include "Pages/Buku/Update_buku.php";
                }
				
				if ($page == 'edit-distributor')
                {
                  include "Pages/Distributor/Update_distributor.php";
                }
				
				if ($page == 'edit-pegawai')
                {
                  include "Pages/Pegawai/Update_pegawai.php";
                }
				
				if ($page == 'input-kerusakan-buku')
                {
                  include "Pages/Buku/Input_kerusakan_buku.php";
                }

				
				if ($page == 'update-stok-buku')
                {
                  include "Pages/Buku/Update_stok_buku.php";
                }


          ?>
        </div><!-- container -->
      </div>
    </div>
        <div class="slim-footer mg-t-0">
          <div class="container-fluid">
            <p>Kompre 2019 &copy; STMIK & POLITEKNIK LPKIA BANDUNG</p>
            <p> <a href="">Riyan Kusuma</a></p>
          </div><!-- container-fluid -->
        </div><!-- slim-footer -->
      </div><!-- slim-mainpanel -->
    </div><!-- slim-body -->
    <style>
        #hilang{
          display: none;
        }
    </style>
    
    <script src="Public/lib/jquery/js/jquery.js"></script>
    <script src="Public/lib/popper.js/js/popper.js"></script>
    <script src="Public/lib/bootstrap/js/bootstrap.js"></script>
    <script src="Public/lib/jquery.cookie/js/jquery.cookie.js"></script>
    <script src="Public/lib/chartist/js/chartist.js"></script>
    <script src="Public/lib/d3/js/d3.js"></script>
    <script src="Public/lib/rickshaw/js/rickshaw.min.js"></script>
    <script src="Public/lib/jquery.sparkline.bower/js/jquery.sparkline.min.js"></script>
    <script src="Public/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

    <script src="Public/js/ResizeSensor.js"></script>
    <script src="Public/js/dashboard.js"></script>
    <script src="Public/js/slim.js"></script>
  </body>
  <script>
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
</html>
