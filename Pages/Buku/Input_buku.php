 <?php 
  include "Model/Model_penerbit.php";
  include "Model/Model_kategori.php";


  $penerbit = new Model_penerbit();
  $kategori = new Model_kategori();
?>

 <div class="section-wrapper">
            <label class="section-title">Form Input Buku</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan buku sesuai dengan data yang ada</p>

            <div class="form-layout">
              <form action="Controllers/Buku/create.php" method="post" enctype="multipart/form-data">
              <div class="row mg-b-25">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Judul Buku: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="judul_buku">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">ISBN : <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="isbn">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Penerbit: <span class="tx-danger">*</span></label>
                    <select class="form-control select2" name="penerbit" data-placeholder="Choose country">
					
                     <?php $result = $penerbit->getData(); ?>
                     <?php while ($data = $result->Fetch_assoc()): ?>
                     <?php $nama = $data['nama']; ?>
                
                    <option value="<?php echo $nama ?>"><?php echo $nama ?></option>
                     <?php endwhile; ?>
                    </select>
                  </div>
                </div><!-- col-4 -->
                  <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Pengarang : <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="pengarang">
                  </div>
                </div><!-- col-4 -->
                   <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Tahun Terbit: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="date" name="tahun_terbit">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Jumlah Halaman: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="number" name="jumlah_halaman">
                  </div>
                </div><!-- col-4 -->
                  
                   <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Cover Buku: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="file" name="gambar">
                  </div>
                </div>
               <div class="col-lg-4">
                  <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Kategori: <span class="tx-danger">*</span></label>
                    <select class="form-control select2" data-placeholder="kategori">
					
                         <?php $result = $kategori->getData(); ?>
                     <?php while ($data2 = $result->Fetch_assoc()): ?>
                     <?php $nama2 = $data2['nama']; ?>
                
                    <option value="<?php echo $nama2 ?>"><?php echo $nama2 ?></option>
                     <?php endwhile; ?>
                    </select>
                  </div>
                </div><!-- col-4 -->
                 <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label">Harga Eceran: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="number" name="harga_eceran">
                  </div>
                </div><!-- col-4 -->
                 <div class="col-lg-2">
                  <div class="form-group">
                    <label class="form-control-label">Stok: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="number" name="stok">
                  </div>
                </div><!-- col-4 -->
                      <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Deskripsi Buku (sinopsis): <span class="tx-danger">*</span></label>
                    <textarea cols="10" rows="9" class="form-control" name="deskripsi"></textarea>
                  </div>
                </div><!-- col-4 -->
                           <div class="col-lg-12">
                  <div class="form-group">
                    
                    <button class="btn btn-primary bd-0">Simpan</button>
					</form>
                  </div>
                </div><!-- col-4 -->
                

              </div><!-- row -->
            </div>
          </div>

          

                </div><!-- row -->

             
              </div><!-- card -->

              <div class="card card-impression mg-t-20">

                <div id="rs3" class="ht-50 ht-sm-70 mg-r--1"></div>
              </div><!-- card -->

          </div><!-- row -->