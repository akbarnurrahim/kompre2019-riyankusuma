<?php
	include_once "/../Config/Database.php";
	
	
	class Model_pegawai {
			
		public function getData()
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * FROM table_pegawai ORDER BY nama_depan ASC";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		
		public function create() 
		{
			$db = new Database();
			$dbConnect = $db->connect();
			
			$sql = "INSERT INTO table_pegawai
					(
						nik,
						nama_depan,
						nama_belakang,
						photo_profile,
						alamat,
						email,
						password,
						jenis_kelamin,
						bagian
					)
					VALUES
					(
						'{$this->nik}',
						'{$this->nama_depan}',
						'{$this->nama_belakang}',
						'{$this->photo_profile}',
						'{$this->alamat}',
						'{$this->email}',
						'{$this->password}',
						'{$this->jenis_kelamin}',
						'{$this->bagian}'
					)";
			
			$data = $dbConnect->query($sql);
			$error = $dbConnect->error;
			$dbConnect = $db->close();
			return $error;
		}
		
		public function hapus()
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "DELETE FROM table_pegawai where id_pegawai = '{$this->id_pegawai}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
		}

		
		
	
	}
	?>