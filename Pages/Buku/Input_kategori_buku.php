 <?php 
  include "/../../Model/Model_kategori.php";
  $buku = new Model_kategori();
?>

 <div class="section-wrapper">
            <label class="section-title">Data Kategori Buku</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan buku sesuai dengan data yang ada</p>

             <div class="form-layout">
              <div class="row mg-b-25">
                <div class="col-lg-12">
				  <form action="Controllers/Kategori/create.php" method="post">
                  <div class="form-group">
                    <label class="form-control-label">Nama Kategori (Mis :Romance): <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="nama">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-12">
                  <div class="form-group">
                    
                    <button style="width:100%;" class="btn btn-primary bd-0">Simpan</button>
                  </div>
                </div><!-- col-4 -->
              </div>
            </form>
          </div>
            <div class="row">
  <div class="col-12 col-sm-12 col-lg-12">
   <div class="table-responsive">
    <table class="table" id="myTable">
     <thead>
      <tr>
        <th width="20px">#</th>
        <th>Kategori
        </th>
        <th>Update</th>
        <th>Hapus</th>
      </tr>
    </thead>
    <tbody>
      <?php $result = $buku->getData(); ?>
      <?php $cek = mysqli_num_rows($result); 
      $i=0;

      ?>
    
      <?php while($data = $result->fetch_array()):  ?>

      <tr>
        <?php $i++ ?>
        <td><?php echo $i ?></td>
        <td><?php echo $data['nama'] ?></td>
       
        <td width="20px"><button class="btn btn-success">Update</button></td>
        <td width="20px"><form action="Controllers/Kategori/delete.php" method="post">
			<input type="hidden" value="<?php echo $data['id_kategori'] ?>" name="id_kategori">
			<button onclick="return confirm('Anda Yakin Akan Menghapus Data ini?');" class="btn btn-danger">hapus</button></form></td>
      </tr>
    <?php endwhile; ?>
    </tbody>
  </table>
  </div>
</div>
