  <?php 
  include "Model/Model_pegawai.php";
  
  $pegawai = new Model_pegawai();
?>
 
 <?php 
	$data = null;
	if (isset($_GET['id_pegawai'])){
		$data = $pegawai->getDetail($_GET['id_pegawai']);
	}
?>
 <div class="section-wrapper">
            <label class="section-title">Form Input Pegawai</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan pegawai sesuai dengan data yang ada</p>

            <div class="form-layout">
              <div class="row mg-b-25">
                <div class="col-lg-6">
				  <form action="Controllers/Pegawai/create.php" method="post">
                  <div class="form-group">
                    <label class="form-control-label">Nama Depan: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="nama_depan">
                  </div>
                </div><!-- col-4 -->
                  <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Nama Belakang: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="nama_belakang">
                  </div>
                </div><!-- col-4 -->
				<div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Nik: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="nik">
                  </div>
                </div><!-- col-4 -->
				         <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Bagian: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="bagian">
                  </div>
                </div><!-- col-4 -->
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Alamat : <span class="tx-danger">*</span></label>
                    <textarea cols="10" rows="5" class="form-control" name="alamat"></textarea>
                  </div>
                </div><!-- col-4 -->
                  <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="email">
                  </div>
                </div><!-- col-4 -->
                   <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Password: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="password" name="password">
                  </div>
                </div><!-- col-4 -->
                   <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Jenis Kelamin: <span class="tx-danger">*</span></label>
                    <select name="jenis_kelamin" class="form-control">
					<option value="1">Laki-Laki</option>
					<option value="2">Perempuan</option>
					</select>
                  </div>
                </div><!-- col-4 -->
         
                 
                           <div class="col-lg-12">
                  <div class="form-group">
                    
                    <button class="btn btn-primary bd-0">Simpan</button>
                  </div>
                </div><!-- col-4 -->
                

              </div><!-- row -->
            </div>
          </div>

          

                </div><!-- row -->

             
              </div><!-- card -->

              <div class="card card-impression mg-t-20">

                <div id="rs3" class="ht-50 ht-sm-70 mg-r--1"></div>
              </div><!-- card -->

          </div><!-- row -->