 <?php 
  include "/../../Model/Model_buku.php";
  $buku = new Model_buku();
?>

 <div class="section-wrapper">
            <label class="section-title">Data Buku</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan buku sesuai dengan data yang ada</p>



           
            <br>
            <div class="row">

  <div class="col-12 col-sm-12 col-lg-12">
   <div class="table-responsive">
    <table class="table" id="myTable">
     <thead>
      <tr>
        <th>#</th>
        <th>Cover
        </th>
        <th>Judul Buku</th>
        <th>ISBN</th>
        <th>Harga</th>
        <th>Detail</th>
        <th>Print</th>
        <th>Update</th>
        <th>Hapus</th>
      </tr>
    </thead>
    <tbody>
      <?php $result = $buku->getData(); ?>
      <?php $cek = mysqli_num_rows($result); 
      $i=0;

      ?>
    
      <?php while($data = $result->fetch_array()):  ?>

      <tr>
        <?php $i++ ?>
        <td><?php echo $i ?></td>
        <td><img src="Img/<?php echo $data['gambar'] ?>"  class="wd-90"></td>
        <td><?php echo $data['judul_buku'] ?></td>
        <td><?php echo $data['isbn'] ?></td>
        <td>Rp.<?php echo $data['harga_eceran'] ?></td>
        <td><button class="btn btn-primary">Lihat Detail</button></td>
        <td><button class="btn btn-warning">Print</button></td>
        <td><form action="index.php?page=edit-buku&id_buku=<?php echo $data['id_buku'] ?>" method="post">
			<button onclick="return confirm('Anda Yakin Akan Menghapus Data ini?');" class="btn btn-success">Edit</button></form>
         <td><form action="Controllers/Buku/delete.php" method="post">
			<input type="hidden" value="<?php echo $data['id_buku'] ?>" name="id_buku">
			<button onclick="return confirm('Anda Yakin Akan Menghapus Data ini?');" class="btn btn-danger">hapus</button></form></td>
      </tr>
    <?php endwhile; ?>
    </tbody>
  </table>
  </div>
</div>
