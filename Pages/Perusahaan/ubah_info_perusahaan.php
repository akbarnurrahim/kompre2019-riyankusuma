  <?php 
  include "Model/Model_perusahaan.php";
  
  $perusahaan = new Model_perusahaan();
?>
 
 <?php 
	$data = null;
	if (isset($_GET['id_perusahaan'])){
		$data = $perusahaan->getDetail($_GET['id_perusahaan']);
	}
?>
  <div class="section-wrapper">
            <label class="section-title">Form Input Pegawai</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan distributor sesuai dengan data yang ada</p>

            <div class="form-layout">
              <div class="row mg-b-25">
                <div class="col-lg-6">
          <form action="Controllers/Pegawai/create.php" method="post">
                  <div class="form-group">
                    <label class="form-control-label">Nama Perusahaan: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="nama" value="<?php echo $data['nama'] ?>">
                  </div>
                </div><!-- col-4 -->
          <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Alamat : <span class="tx-danger">*</span></label>
                    <textarea cols="10" rows="5" class="form-control" name="alamat"><?php echo $data['alamat'] ?></textarea>
                  </div>
           <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Kode POS: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="kode_pos" value="<?php echo $data['nama'] ?>">
                  </div>
                </div><!-- col-4 -->
           </div><!-- col-4 -->
                  <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Website: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="website" value="<?php echo $data['nama'] ?>">
                  </div>
           <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="email" name="email" value="<?php echo $data['email'] ?>">
                  </div>
                </div><!-- col-4 -->
                </div><!-- col-4 -->
        <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Telepon: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="nik" value="<?php echo $data['telepon'] ?>">
                  </div>
                </div><!-- col-4 -->

                           <div class="col-lg-12">
                  <div class="form-group">
                    
                    <button class="btn btn-primary bd-0">Simpan</button>
                  </form>
                  </div>
                </div><!-- col-4 -->
                

              </div><!-- row -->
            </div>
          </div>

          

                </div><!-- row -->

             
              </div><!-- card -->

              <div class="card card-impression mg-t-20">

                <div id="rs3" class="ht-50 ht-sm-70 mg-r--1"></div>
              </div><!-- card -->

          </div><!-- row -->