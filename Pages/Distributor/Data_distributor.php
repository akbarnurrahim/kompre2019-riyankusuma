 <?php 
  include "Model/Model_distributor.php";
  $distributor = new Model_distributor();
?>

 <div class="section-wrapper">
            <label class="section-title">Data distributor</label>
            <p class="mg-b-20 mg-sm-b-40">Inputkan distributor sesuai dengan data yang ada</p>
            <div class="row">
  <div class="col-12 col-sm-12 col-lg-12">
   <div class="table-responsive">
    <table class="table" id="myTable">
     <thead>
      <tr>
        <th>#</th>
        <th>Nama Distributor</th>
        <th>Alamat</th>
        <th>Telepon</th>
        <th>Nomor Whatsapp</th>
        <th>Email</th>
        <th>Print</th>
        <th>Update</th>
        <th>Hapus</th>
      </tr>
    </thead>
    <tbody>
      <?php $result = $distributor->getData(); ?>
      <?php $cek = mysqli_num_rows($result); 
      $i=0;

      ?>
    
      <?php while($data = $result->fetch_array()):  ?>

      <tr>
        <?php $i++ ?>
        <td><?php echo $i ?></td>
        <td><?php echo $data['nama'] ?></td>
        <td><?php echo $data['alamat'] ?></td>
        <td><?php echo $data['telepon'] ?></td>
        <td><?php echo $data['whatsapp'] ?></td>
        <td><?php echo $data['email'] ?></td>
        <td><button class="btn btn-warning">Print</button></td>
        <td><form action="index.php?page=edit-distributor&id_distributor=<?php echo $data['id_distributor'] ?>" method="post">
			<button onclick="return confirm('Anda Yakin Akan Menghapus Data ini?');" class="btn btn-success">Edit</button></form></td>
        <td><form action="Controllers/Distributor/delete.php" method="post">
			<input type="hidden" value="<?php echo $data['id_distributor'] ?>" name="id_distributor">
			<button onclick="return confirm('Anda Yakin Akan Menghapus Data ini?');" class="btn btn-danger">Hapus</button></form></td>
      </tr>
    <?php endwhile; ?>
    </tbody>
  </table>
  </div>
</div>
