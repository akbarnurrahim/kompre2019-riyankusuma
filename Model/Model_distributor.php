<?php
	include_once "/../Config/Database.php";
	
	
	class Model_distributor {
			
		public function getData()
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * FROM table_distributor ORDER BY nama ASC";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		
		public function create() 
		{
			$db = new Database();
			$dbConnect = $db->connect();
			
			$sql = "INSERT INTO table_distributor
					(
						nama,
						alamat,
						telepon,
						whatsapp,
						email
					)
					VALUES
					(
						'{$this->nama}',
						'{$this->alamat}',
						'{$this->telepon}',
						'{$this->whatsapp}',
						'{$this->email}'
					)";
			
			$data = $dbConnect->query($sql);
			$error = $dbConnect->error;
			$dbConnect = $db->close();
			return $error;
		}
		
		public function hapus()
		{
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "DELETE FROM table_distributor where id_distributor = '{$this->id_distributor}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
		}

		
		
	
	}
	?>