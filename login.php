<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Bukuku. Login</title>

    <!-- Vendor css -->
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
    <link href="Public/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="Public/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="Public/css/slim.css">

  </head>
  <body>

    <div class="signin-wrapper">

      <div class="signin-box">
        <h2 class="slim-logo"><a href="index.html">Bukuku<span>.</span></a></h2>
        <h2 class="signin-title-primary">Selamat Datang Kembali !</h2>
        <h3 class="signin-title-secondary">Login untuk melanjutkan.</h3>
        <form action="Controllers/Login/auth.php" method="post" >
        <div class="form-group">
          <input type="text" class="form-control" name="email" placeholder="Masukan Email">
        </div><!-- form-group -->
        <div class="form-group mg-b-50">
          <input type="password" class="form-control"  name="password" placeholder="Masukan Password">
        </div><!-- form-group -->
        <button class="btn btn-primary btn-block btn-signin">Masuk</button>
        <p class="mg-b-0">Butuh Bantuan ? <a href="page-signup.html">Pusat Bantuan</a></p>
      </div><!-- signin-box -->
        </form>
    </div><!-- signin-wrapper -->

    <script src="Public/lib/jquery/js/jquery.js"></script>
    <script src="Public/lib/popper.js/js/popper.js"></script>
    <script src="Public/lib/bootstrap/js/bootstrap.js"></script>

    <script src="Public/js/slim.js"></script>

  </body>
</html>
